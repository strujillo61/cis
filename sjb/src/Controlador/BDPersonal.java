/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import conexion.ConexionBD;
import static conexion.ConexionBD.connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import modelo.Personal;

/**
 *
 * @author sebas
 */
public class BDPersonal extends ConexionBD {

    public BDPersonal() {
        super();
    }

    private final String TABLE_NAME = "personal";

    public void insertar(Personal personal) {
        try {
            conectar();

            String query = "insert into " + TABLE_NAME + " values (?, ?, ?, ?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            int posicion = 0;
            preparedStatement.setString(++posicion, personal.getCodigo());
            preparedStatement.setInt(++posicion, personal.getDocumento());
            preparedStatement.setString(++posicion, personal.getNombre());
            preparedStatement.setString(++posicion, personal.getDependencia());
            preparedStatement.executeUpdate();
            preparedStatement.close();

        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "error en la consulta sql " + e.getMessage());
        } finally {
            desconectar();
        }
    }

    public List<Personal> consultarTodos() {
        List<Personal> resultado = new ArrayList<>();

        try {
            conectar();
            String query = "select * from " + TABLE_NAME;
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {

                Personal personal = new Personal();

                personal.setCodigo(resultSet.getString("codigo"));
                personal.setDocumento(resultSet.getInt("documento"));
                personal.setNombre(resultSet.getString("nombre"));
                personal.setDependencia(resultSet.getString("dependencia"));

                resultado.add(personal);

            }
            resultSet.close();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "error en la consulta sql " + e.getMessage());
        } finally {
            desconectar();
        }
        return resultado;
    }

    public void eliminarPersonal(String codigo) {

        try {
            conectar();

            String query = "delete from " + TABLE_NAME + " where codigo=?";
            PreparedStatement preparedstatement = connection.prepareStatement(query);
            preparedstatement.setString(1, codigo);
            preparedstatement.executeUpdate();
            preparedstatement.close();
            
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            JOptionPane.showMessageDialog(null, "No se pudo conectar a la base de datos " + e.getMessage());
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error en la consulta SQL " + e.getMessage());
        } finally {
            desconectar();
        }

    }
    
    

}
