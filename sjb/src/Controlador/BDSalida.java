/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import conexion.ConexionBD;
import static conexion.ConexionBD.connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import modelo.Ingreso;
import modelo.Salida;

/**
 *
 * @author sebas
 */
public class BDSalida extends ConexionBD {

    public BDSalida() {
        super();
    }

    private final String TABLE_NAME = "salida";

    public void insertar(Salida salida) {
        try {
            conectar();

            String query = "insert into " + TABLE_NAME + " values (?, ?, ?, ?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            int posicion = 0;
            preparedStatement.setInt(++posicion, salida.getId_salida());
            preparedStatement.setString(++posicion, salida.getCodigo_personal());
            preparedStatement.setString(++posicion, salida.getFecha());
            preparedStatement.setString(++posicion, salida.getHora_salida());
            preparedStatement.executeUpdate();
            
            preparedStatement.close();
            
            

        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "el usuario que esta ingresando, no existe en el sistema. ");
        } finally {
           
            desconectar();
        }
    }

}
