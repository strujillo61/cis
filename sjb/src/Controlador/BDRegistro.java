/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import conexion.ConexionBD;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import modelo.Registros;

/**
 *
 * @author sebas
 */
public class BDRegistro extends ConexionBD {

    public BDRegistro() {
        super();
    }

    public List<Registros> consultarTodos() {
        List<Registros> resultado = new ArrayList<>();

        try {
            conectar();
            String query = "SELECT personal.documento, personal.nombre, personal.dependencia, ingreso_salida.codigo_per, \n"
                    + "ingreso_salida.fecha, ingreso_salida.hora_ingreso, ingreso_salida.hora_salida FROM ingreso_salida, personal\n"
                    + "WHERE ingreso_salida.codigo_per  = personal.codigo";
            
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {

                Registros registros = new Registros();
                registros.setDocumento(resultSet.getInt("documento"));
                registros.setNombre(resultSet.getString("nombre"));
                registros.setDependencia(resultSet.getString("dependencia"));
                registros.setCodigo(resultSet.getString("codigo_per"));
                registros.setFecha(resultSet.getString("fecha"));
                registros.setHora_ingreso(resultSet.getString("hora_ingreso"));
                registros.setHora_salida(resultSet.getString("hora_salida"));
                
                

                resultado.add(registros);

            }
            resultSet.close();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "error en la consulta sql " + e.getMessage());
        } finally {
            desconectar();
        }
        return resultado;
    }
    
    //crear el metodo del delete datos de la tbla 
     public  void eliminarTodos() {
      

        try {
            conectar();
            String query = "DELETE  FROM ingreso_salida";
            
            Statement st = connection.createStatement();
            st.execute(query);


            
            
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "error en la consulta sql " + e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (Throwable t) {
            }
            desconectar();
        }
        
    }
     
     public  void resetearAutoincrement() {
      

        try {
            conectar();
            String query = "alter table ingreso_salida AUTO_INCREMENT=1";
            
            Statement st = connection.createStatement();
            st.execute(query);
            
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "error en la consulta sql " + e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (Throwable t) {
            }
            desconectar();
        }
        
    }
    
}
