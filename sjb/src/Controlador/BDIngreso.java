/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import conexion.ConexionBD;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import modelo.Ingreso;

/**
 *
 * @author sebas
 */
public class BDIngreso extends ConexionBD {

    public BDIngreso() {
        super();
    }

    private final String TABLE_NAME = "ingreso";

    public void insertar(Ingreso ingreso) {
        try {
            conectar();

            String query = "insert into " + TABLE_NAME + " values (?, ?, ?, ?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            int posicion = 0;
            preparedStatement.setInt(++posicion, ingreso.getId_ingreso());
            preparedStatement.setString(++posicion, ingreso.getCodigo_personal());
            preparedStatement.setString(++posicion, ingreso.getFecha());
            preparedStatement.setString(++posicion, ingreso.getHora_ingreso());
            preparedStatement.executeUpdate();

            preparedStatement.close();

        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "el usuario que esta ingresando, no existe en el sistema. ");
        } finally {
            desconectar();
        }
    }

   
}
