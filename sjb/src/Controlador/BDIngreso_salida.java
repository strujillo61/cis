/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import conexion.ConexionBD;
import static conexion.ConexionBD.connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import modelo.Ingreso_salida;

/**
 *
 * @author Enfermeria
 */
public class BDIngreso_salida extends ConexionBD {

    public BDIngreso_salida() {
        super();
    }

    private final String TABLE_NAME = "ingreso_salida";

    public void insertar(Ingreso_salida ingreso_salida) {
        try {
            conectar();

            String query = "insert into " + TABLE_NAME + " values (?, ?, ?, ?,?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            int posicion = 0;

            preparedStatement.setInt(++posicion, ingreso_salida.getId());
            preparedStatement.setString(++posicion, ingreso_salida.getCodigo_per());
            preparedStatement.setString(++posicion, ingreso_salida.getFecha());
            preparedStatement.setString(++posicion, ingreso_salida.getHora_ingreso());
            preparedStatement.setString(++posicion, ingreso_salida.getHora_salida());
            preparedStatement.executeUpdate();
            preparedStatement.close();

        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "el usuario que esta ingresando, no existe en el sistema. ");
        } finally {
            desconectar();
        }

    }

    public void Actualizar(String hora_salida, String fecha, String codigo_per) {

        try {
            conectar();
            String query = "UPDATE " + TABLE_NAME + " SET hora_salida=? WHERE fecha=DATE_FORMAT(NOW(), "
                    + "'%d/%m/%Y')= fecha=? AND codigo_per=?";

            PreparedStatement preparedStatement = connection.prepareStatement(query);

            preparedStatement.setString(1, hora_salida);
            preparedStatement.setString(2, fecha);
            preparedStatement.setString(3, codigo_per);
            preparedStatement.executeUpdate();

        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            JOptionPane.showMessageDialog(null, e);
        } catch (SQLException e) {

           JOptionPane.showMessageDialog(null, "el usuario que esta intentando salir, no existe en el sistema. ");

        } finally {

            desconectar();

        }

    }

    public void Actualiza_version2(Ingreso_salida ingreso_salida) {
        try {
            conectar();

            String query = "UPDATE " + TABLE_NAME + " SET hora_salida=? WHERE fecha=DATE_FORMAT(NOW(), "
                    + "'%d/%m/%Y')= fecha=? AND codigo_per=?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            int posicion = 0;

            preparedStatement.setString(++posicion, ingreso_salida.getHora_salida());
            preparedStatement.setString(++posicion, ingreso_salida.getFecha());
            preparedStatement.setString(++posicion, ingreso_salida.getCodigo_per());

            preparedStatement.executeUpdate();
            preparedStatement.close();

        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "el usuario que esta ingresando, no existe en el sistema. ");
        } finally {
            desconectar();
        }

    }

    public void actualizarSuscriptor(Ingreso_salida ingreso_salida) {

        try {
            conectar();
             String query = "UPDATE "+TABLE_NAME+" SET codigo_per=?, fecha=?,"
                     + " hora_salida=? WHERE fecha=? AND codigo_per=?";
             PreparedStatement preparedStatement = connection.prepareStatement(query);
            
            int pos = 0;
            preparedStatement.setString(++pos, ingreso_salida.getCodigo_per());
            preparedStatement.setString(++pos, ingreso_salida.getFecha());
           
            preparedStatement.setString(++pos, ingreso_salida.getHora_salida());
            preparedStatement.setString(++pos, ingreso_salida.getFecha());
            preparedStatement.setString(++pos, ingreso_salida.getCodigo_per());
            preparedStatement.executeUpdate();
            preparedStatement.close();
           
          
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            JOptionPane.showMessageDialog(null, "No se pudo conectar a la base de datos " + e.getMessage());
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error en la consulta SQL " + e.getMessage());
        } finally {
            desconectar();
        }

    }

}
