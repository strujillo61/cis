/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;

import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author sebas
 */
public class NewClass {
    public static void main(String[] args) {
        //probamos la conexion  a la base de datos
        
        try {
            ConexionBD conexion = new ConexionBD();
            conexion.conectar();
            conexion.desconectar();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | SQLException e) {
            JOptionPane.showMessageDialog(null,"NO SE PUEDE CONECTAR"+ e);
        }
    }
    
}
