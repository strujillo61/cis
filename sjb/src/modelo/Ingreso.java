/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author sebas
 */
public class Ingreso {
    
   private  int id_ingreso;
   private  String codigo_personal;
   private  String fecha;
   private  String hora_ingreso;

    public Ingreso() {
    }

    public Ingreso(int id_ingreso, String codigo_personal, String fecha, String hora_ingreso) {
        this.id_ingreso = id_ingreso;
        this.codigo_personal = codigo_personal;
        this.fecha = fecha;
        this.hora_ingreso = hora_ingreso;
    }

    public int getId_ingreso() {
        return id_ingreso;
    }

    public void setId_ingreso(int id_ingreso) {
        this.id_ingreso = id_ingreso;
    }

    public String getCodigo_personal() {
        return codigo_personal;
    }

    public void setCodigo_personal(String codigo_personal) {
        this.codigo_personal = codigo_personal;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora_ingreso() {
        return hora_ingreso;
    }

    public void setHora_ingreso(String hora_ingreso) {
        this.hora_ingreso = hora_ingreso;
    }

    
}
