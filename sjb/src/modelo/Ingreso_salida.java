/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Enfermeria
 */
public class Ingreso_salida {
    private int id;
    private String codigo_per;
    private  String fecha;
    private  String hora_ingreso;
    private String hora_salida;

    public Ingreso_salida() {
    }

    public Ingreso_salida(int id, String codigo_per, String fecha, String hora_ingreso, String hora_salida) {
        this.id = id;
        this.codigo_per = codigo_per;
        this.fecha = fecha;
        this.hora_ingreso = hora_ingreso;
        this.hora_salida = hora_salida;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCodigo_per() {
        return codigo_per;
    }

    public void setCodigo_per(String codigo_per) {
        this.codigo_per = codigo_per;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora_ingreso() {
        return hora_ingreso;
    }

    public void setHora_ingreso(String hora_ingreso) {
        this.hora_ingreso = hora_ingreso;
    }

    public String getHora_salida() {
        return hora_salida;
    }

    public void setHora_salida(String hora_salida) {
        this.hora_salida = hora_salida;
    }
    
    
}
