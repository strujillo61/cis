/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author sebas
 */
public class Salida {
    
    private int id_salida;
    private String codigo_personal;
    private String fecha;
    private String hora_salida;

    public Salida() {
    }

    public Salida(int id_salida, String codigo_personal, String fecha, String hora_salida) {
        this.id_salida = id_salida;
        this.codigo_personal = codigo_personal;
        this.fecha = fecha;
        this.hora_salida = hora_salida;
    }

    public int getId_salida() {
        return id_salida;
    }

    public void setId_salida(int id_salida) {
        this.id_salida = id_salida;
    }

    public String getCodigo_personal() {
        return codigo_personal;
    }

    public void setCodigo_personal(String codigo_personal) {
        this.codigo_personal = codigo_personal;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora_salida() {
        return hora_salida;
    }

    public void setHora_salida(String hora_salida) {
        this.hora_salida = hora_salida;
    }
    
    
    
    
}
