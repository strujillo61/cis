-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.4.18-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Volcando datos para la tabla sjb.ingreso: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `ingreso` DISABLE KEYS */;
INSERT INTO `ingreso` (`id_ingreso`, `codigo_personal`, `fecha`, `hora_ingreso`) VALUES
	(2, '12', '12', '121');
/*!40000 ALTER TABLE `ingreso` ENABLE KEYS */;

-- Volcando datos para la tabla sjb.ingreso_salida: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `ingreso_salida` DISABLE KEYS */;
/*!40000 ALTER TABLE `ingreso_salida` ENABLE KEYS */;

-- Volcando datos para la tabla sjb.personal: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `personal` DISABLE KEYS */;
INSERT INTO `personal` (`codigo`, `documento`, `nombre`, `dependencia`) VALUES
	('12', 12, '12', '12');
/*!40000 ALTER TABLE `personal` ENABLE KEYS */;

-- Volcando datos para la tabla sjb.salida: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `salida` DISABLE KEYS */;
INSERT INTO `salida` (`id_salida`, `codigo_personal`, `fecha`, `hora_salida`) VALUES
	(25, '12', '12', '121');
/*!40000 ALTER TABLE `salida` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
