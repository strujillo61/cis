-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.4.18-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Volcando estructura de base de datos para sjb
CREATE DATABASE IF NOT EXISTS `sjb` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci */;
USE `sjb`;

-- Volcando estructura para tabla sjb.ingreso
CREATE TABLE IF NOT EXISTS `ingreso` (
  `id_ingreso` int(255) NOT NULL AUTO_INCREMENT,
  `codigo_personal` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `fecha` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `hora_ingreso` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id_ingreso`),
  KEY `FKPersonal` (`codigo_personal`),
  CONSTRAINT `FKPersonal` FOREIGN KEY (`codigo_personal`) REFERENCES `personal` (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla sjb.ingreso: ~1 rows (aproximadamente)
DELETE FROM `ingreso`;
/*!40000 ALTER TABLE `ingreso` DISABLE KEYS */;
INSERT INTO `ingreso` (`id_ingreso`, `codigo_personal`, `fecha`, `hora_ingreso`) VALUES
	(2, '12', '12', '121');
/*!40000 ALTER TABLE `ingreso` ENABLE KEYS */;

-- Volcando estructura para tabla sjb.ingreso_salida
CREATE TABLE IF NOT EXISTS `ingreso_salida` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `codigo_per` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `fecha` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `hora_ingreso` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `hora_salida` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `codigo_per` (`codigo_per`),
  CONSTRAINT `FKcodigo` FOREIGN KEY (`codigo_per`) REFERENCES `personal` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla sjb.ingreso_salida: ~0 rows (aproximadamente)
DELETE FROM `ingreso_salida`;
/*!40000 ALTER TABLE `ingreso_salida` DISABLE KEYS */;
/*!40000 ALTER TABLE `ingreso_salida` ENABLE KEYS */;

-- Volcando estructura para tabla sjb.personal
CREATE TABLE IF NOT EXISTS `personal` (
  `codigo` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `documento` int(50) DEFAULT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `dependencia` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla sjb.personal: ~1 rows (aproximadamente)
DELETE FROM `personal`;
/*!40000 ALTER TABLE `personal` DISABLE KEYS */;
INSERT INTO `personal` (`codigo`, `documento`, `nombre`, `dependencia`) VALUES
	('12', 12, '12', '12');
/*!40000 ALTER TABLE `personal` ENABLE KEYS */;

-- Volcando estructura para tabla sjb.salida
CREATE TABLE IF NOT EXISTS `salida` (
  `id_salida` int(255) NOT NULL AUTO_INCREMENT,
  `codigo_personal` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `fecha` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `hora_salida` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id_salida`) USING BTREE,
  KEY `codigo_personal` (`codigo_personal`),
  CONSTRAINT `FKCodi_personal` FOREIGN KEY (`codigo_personal`) REFERENCES `personal` (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla sjb.salida: ~1 rows (aproximadamente)
DELETE FROM `salida`;
/*!40000 ALTER TABLE `salida` DISABLE KEYS */;
INSERT INTO `salida` (`id_salida`, `codigo_personal`, `fecha`, `hora_salida`) VALUES
	(25, '12', '12', '121');
/*!40000 ALTER TABLE `salida` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
